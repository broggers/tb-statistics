build-macos:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build  -o get-stat${VER} ./cmd/tb-statistics

build-win:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build  -o get-stat${VER}.exe ./cmd/tb-statistics


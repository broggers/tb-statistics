package main

import (
	"fmt"
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
	"gitlab.com/ogornostaev/tb-statistics/internal/stats"
	"log"
	"time"
)

func main() {

	if len(config.DateFor) > 0 && len(config.DateFrom) > 0 {
		log.Fatalln("Only one parameter must be set: --from or --date")
	} else if len(config.DateFrom) > 0 {
		dateFrom := config.DateFrom

		fmt.Println("Print stat from: ", dateFrom)
		stats.PrintStatFrom(dateFrom, config.Tickers)
	} else {
		var dateFor string

		if len(config.DateFor) == 0 {
			dateFor = time.Now().UTC().Format(config.DateFormat)
		} else {
			dateFor = config.DateFor
		}

		log.Println("Print stat for date: ", dateFor)
		stats.PrintStatForDate(dateFor, config.Tickers)
	}

	fmt.Println("Bye bye!")
}

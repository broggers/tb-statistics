package config

import (
	"bufio"
	"context"
	sdk "github.com/TinkoffCreditSystems/invest-openapi-go-sdk"
	"io"
	"log"
	"math/rand"
	"os"
	"reflect"
	"strings"
	"time"
)

const (
	UsdTicker     = "USD000UTSTOM"
	TimeFormat    = "2006-01-02T15:04:05"
	TimeFormatExt = "2006-01-02 15:04:05"
	DateFormat    = "2006-01-02"
)

var (
	DateFrom = getArg(os.Args, "--from")
	DateTo   = getArg(os.Args, "--to")
	DateFor  = getArg(os.Args, "--date")

	Debug  = contains(os.Args, "--debug")
	Dry    = contains(os.Args, "--dry")
	Closed = contains(os.Args, "--closed")

	token          string
	tokenFromParam = getArg(os.Args, "--key")
	tokenFile      = getArgOrDefault(os.Args, "--conf", "./key.conf")

	Tickers = getArrayArg(os.Args, "--ticker")

	client           *sdk.RestClient
	clientCtx        context.Context
	clientCancelFunc context.CancelFunc

	StartTradesTime = time.Date(2018, 1, 1, 10, 0, 0, 0, time.UTC)
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func GetDate(date string) time.Time {
	layout := DateFormat
	t, err := time.Parse(layout, date)

	if err != nil {
		log.Fatalln(err)
	}
	return t
}

func GetTime(date string) time.Time {
	layout := TimeFormat
	t, err := time.Parse(layout, date)

	if err != nil {
		log.Fatalln(err)
	}
	return t
}

func GetClient() (_client *sdk.RestClient, _clientCtx context.Context, _clientCancelFunc context.CancelFunc) {

	tokenFromFile := getLineFromFile(tokenFile)
	if len(tokenFromFile) != 0 {
		token = tokenFromFile
	} else {
		token = tokenFromParam
	}

	if len(token) == 0 {
		log.Fatalln("Must be set API token via --key parameter or --conf file (default key.conf)")
	}

	client = sdk.NewRestClient(token)
	clientCtx, clientCancelFunc = context.WithTimeout(context.Background(), 5*time.Minute)
	return client, clientCtx, clientCancelFunc
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func contains(args []string, arg string) bool {
	for _, a := range args {
		if a == arg {
			return true
		}
	}
	return false
}

func getArg(args []string, arg string) string {
	for i, a := range args {
		if a == arg {
			return args[i+1]
		}
	}
	return ""
}

func getArrayArg(args []string, arg string) []string {
	arrayValue := ""
	for i, a := range args {
		if a == arg {
			arrayValue = args[i+1]
			break
		}
	}

	if len(arrayValue) == 0 {
		return []string{}
	} else {
		return strings.Split(arrayValue, ",")
	}
}

func getArgOrDefault(args []string, arg string, fallback string) string {
	for i, a := range args {
		if a == arg {
			return args[i+1]
		}
	}
	return fallback
}

func IfThenElse(condition bool, a interface{}, b interface{}) interface{} {
	if condition {
		return a
	}
	return b
}

func GetFloat(unk interface{}) float64 {
	floatType := reflect.TypeOf(float64(0))

	v := reflect.ValueOf(unk)
	v = reflect.Indirect(v)
	if !v.Type().ConvertibleTo(floatType) {
		return -1.0
	}
	fv := v.Convert(floatType)
	return fv.Float()
}

func GetString(unk interface{}) string {
	stringType := reflect.TypeOf("")

	v := reflect.ValueOf(unk)
	v = reflect.Indirect(v)
	if !v.Type().ConvertibleTo(stringType) {
		return ""
	}
	fv := v.Convert(stringType)
	return fv.String()
}

func getLineFromFile(f string) string {
	fi, err := os.Stat(f)
	if os.IsNotExist(err) {
		return ""
	}

	if err != nil {
		log.Println("Unable to get info about file:", f, err)
		return ""
	}

	if fi.IsDir() {
		log.Println("Key file can't be a dir: ", f)
		return ""
	}

	size := fi.Size()
	if size > 256 {
		log.Println("Key file so large: ", f, "must be less: ", 256)
		return ""
	}

	file, err := os.Open(f)
	if err != nil {
		log.Println("Unable to open file:", f, err)
		return ""
	}
	defer file.Close()

	// Start reading from the file with a reader.
	reader := bufio.NewReader(file)
	var line string
	for {
		line, err = reader.ReadString('\n')
		if err != nil && err != io.EOF {
			break
		}

		return strings.ReplaceAll(line, "\n", "")
	}
	if err != io.EOF {
		log.Println("Unable to read file: ", f, err)
		return ""
	}

	return ""
}

package stats

import (
	"fmt"
	"github.com/cheggaaa/pb/v3"
	"github.com/gookit/color"
	"github.com/jedib0t/go-pretty/table"
	"github.com/jedib0t/go-pretty/text"
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
	"gitlab.com/ogornostaev/tb-statistics/internal/deals"
	"gitlab.com/ogornostaev/tb-statistics/internal/helper"
	"log"
	"os"
	"time"
)

var (
	red   = color.Red.Render
	green = color.Green.Render
	white = color.White.Render

	closeDeal = color.White.Render
	openDeal  = color.BgDarkGray.Render
)

func PrintStatFrom(date string, tickers []string) {

	t := config.GetDate(date)

	from := time.Date(t.Year(), t.Month(), t.Day(), 7, 0, 0, 0, time.UTC)
	to := time.Now().UTC()

	PrintStat(from, to, tickers)
}

func PrintStatForDate(date string, tickers []string) {

	t := config.GetDate(date)

	from := time.Date(t.Year(), t.Month(), t.Day(), 7, 0, 0, 0, time.UTC)
	to := from.Add(16 * time.Hour)

	PrintStat(from, to, tickers)
}

func PrintStat(from time.Time, to time.Time, _tickers []string) {

	os.Stdout.WriteString("\n")
	bar := pb.StartNew(100)

	bar.Start()
	bar.Add(5)

	var tickers []string
	if len(_tickers) == 0 {
		allTickers, err := helper.GetUniqueTickers(from, to)

		if err != nil {
			log.Fatalln("Unable to get tickers list", err)
		}

		tickers = allTickers
	} else {
		tickers = _tickers
	}

	bar.Add(5)

	barTickerWeight := 0
	if len(tickers) > 0 {
		barTickerWeight = 80 / len(tickers)
	}

	totalUsd := 0.0
	totalCommissionUsd := 0.0
	totalDealsUsd := 0
	rowsClosedUsd := make([]table.Row, 0)

	totalRub := 0.0
	totalCommissionRub := 0.0
	totalDealsRub := 0
	rowsClosedRub := make([]table.Row, 0)

	rowsClosedDeals := make([]table.Row, 0)
	rowsUnclosed := make([]table.Row, 0)

	for _, ticker := range tickers {

		if ticker == "USD000UTSTOM" || ticker == "EUR_RUB__TOM" {
			bar.Add(barTickerWeight)
			continue
		}

		getDealsMethod := deals.GetDealsFilteredByOpenTime
		if config.Closed {
			getDealsMethod = deals.GetDealsFilteredByCloseTime
		}

		dealsInfo, tradesInfo, err := getDealsMethod(ticker,
			from,
			to)

		if err != nil {
			log.Fatalln(err)
		}

		profit := 0.0
		commission := 0.0
		amount := 0
		avgPrice := 0.0
		avgPriceZP := 0.0

		if tradesInfo.GetTradesCount() > 0 {
			amount = tradesInfo.GetAmount()
			avgPrice = tradesInfo.GetAvgPrice()
		}

		if dealsInfo.GetDealsCount() > 0 {

			if dealsInfo.IsUSD() {
				totalUsd += dealsInfo.GetProfit()
				totalCommissionUsd += dealsInfo.GetCommission()
				totalDealsUsd += dealsInfo.GetDealsCount()
			} else {
				totalRub += dealsInfo.GetProfit()
				totalCommissionRub += dealsInfo.GetCommission()
				totalDealsRub += dealsInfo.GetDealsCount()
			}

			profit = dealsInfo.GetProfit()
			commission = dealsInfo.GetCommission()
			avgPriceZP = dealsInfo.GetZeroProfitAvgPrice(tradesInfo)
		}

		if dealsInfo.GetProfit() != 0 {
			if dealsInfo.IsUSD() {
				rowsClosedUsd = append(rowsClosedUsd, table.Row{ticker,
					value(profit, dealsInfo.IsUSD(), false, true),
					value(commission, dealsInfo.IsUSD(), false, false),
					dealsInfo.GetDealsCount(),
					amount,
					value(avgPrice, dealsInfo.IsUSD(), true, false),
					value(avgPriceZP, dealsInfo.IsUSD(), true, false)})
			} else {
				rowsClosedRub = append(rowsClosedRub, table.Row{ticker,
					value(profit, dealsInfo.IsUSD(), false, true),
					value(commission, dealsInfo.IsUSD(), false, false),
					dealsInfo.GetDealsCount(),
					amount,
					value(avgPrice, dealsInfo.IsUSD(), true, false),
					value(avgPriceZP, dealsInfo.IsUSD(), true, false)})
			}
		}

		for _, d := range dealsInfo.GetDeals() {

			buyColor := openDeal
			sellColor := closeDeal

			dealDirection := green("->")
			if !d.IsLongDeal() {
				dealDirection = red("<-")

				buyColor = closeDeal
				sellColor = openDeal
			}

			rowsClosedDeals = append(rowsClosedDeals, table.Row{
				ticker,
				d.GetBuyTrade().GetAmount(),

				buyColor(d.GetBuyTrade().GetTime().Format(config.TimeFormatExt)),
				buyColor(value(d.GetBuyTrade().GetPrice(), dealsInfo.IsUSD(), false, false)),
				buyColor(value(d.GetBuyTrade().GetCommission(), dealsInfo.IsUSD(), false, false)),
				buyColor(value(d.GetBuyTrade().GetCostWithCommission(), dealsInfo.IsUSD(), false, false)),
				dealDirection,
				sellColor(d.GetSellTrade().GetTime().Format(config.TimeFormatExt)),
				sellColor(value(d.GetSellTrade().GetPrice(), dealsInfo.IsUSD(), false, false)),
				sellColor(value(d.GetSellTrade().GetCommission(), dealsInfo.IsUSD(), false, false)),
				sellColor(value(d.GetSellTrade().GetCostWithCommission(), dealsInfo.IsUSD(), false, false)),

				value(d.GetProfit(), dealsInfo.IsUSD(), false, true),
			})
		}

		if dealsInfo.GetDealsCount() > 0 {
			rowsClosedDeals = append(rowsClosedDeals,
				table.Row{"_____", "________",
					"___________________", "_________", "______", "_________",
					"__",
					"___________________", "_________", "______", "_________",
					"_________"})
		}

		if tradesInfo.GetTradesCount() > 0 {
			for _, t := range tradesInfo.GetTrades() {
				rowsUnclosed = append(rowsUnclosed, table.Row{ticker,
					t.GetTime().Format(config.TimeFormatExt),
					value(t.GetPrice(), dealsInfo.IsUSD(), false, false),
					t.GetAmount(),
					t.GetCommission()})
			}

			rowsUnclosed = append(rowsUnclosed, table.Row{"",
				"TOTAL: ",
				value(tradesInfo.GetAvgPrice(), dealsInfo.IsUSD(), false, false),
				tradesInfo.GetAmount(),
				value(tradesInfo.GetCommission(), dealsInfo.IsUSD(), false, false)})

			rowsUnclosed = append(rowsUnclosed, table.Row{"_________",
				"___________________",
				"__________",
				"__________",
				"__________"})
		}

		bar.Add(barTickerWeight)
	}

	bar.Add(100 - int(bar.Current()))
	bar.Finish()
	os.Stdout.WriteString("\n")

	if len(rowsClosedUsd) > 0 {
		tblUsd := table.NewWriter()
		tblUsd.SetColumnConfigs([]table.ColumnConfig{
			{
				Name:        "Ticker",
				Align:       text.AlignLeft,
				AlignFooter: text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Profit",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Commission",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Deals",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Amount",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Avg_Price",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Avg_Price_ZP",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
		})
		tblUsd.SetOutputMirror(os.Stdout)
		tblUsd.AppendHeader(table.Row{"Ticker", "Profit", "Commission", "Deals", "Amount", "Avg_Price", "Avg_Price_ZP"})

		for _, row := range rowsClosedUsd {
			tblUsd.AppendRow(row)
		}

		tblUsd.AppendFooter(table.Row{"Total",
			value(totalUsd, true, false, false),
			value(totalCommissionUsd, true, false, false),
			totalDealsUsd, "", "", ""})

		os.Stdout.WriteString("Closed Deals, USD\n")
		tblUsd.Render()
		os.Stdout.WriteString("\n\n")
	}

	if len(rowsClosedRub) > 0 {
		tblRub := table.NewWriter()
		tblRub.SetColumnConfigs([]table.ColumnConfig{
			{
				Name:        "Ticker",
				Align:       text.AlignLeft,
				AlignFooter: text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Profit",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Commission",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Deals",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Amount",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Avg_Price",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Avg_Price_ZP",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
		})
		tblRub.SetOutputMirror(os.Stdout)
		tblRub.AppendHeader(table.Row{"Ticker", "Profit", "Commission", "Deals", "Amount", "Avg_Price", "Avg_Price_ZP"})

		for _, row := range rowsClosedRub {
			tblRub.AppendRow(row)
		}

		tblRub.AppendFooter(table.Row{"Total",
			value(totalRub, false, false, false),
			value(totalCommissionRub, true, false, false),
			totalDealsRub, "", "", ""})

		os.Stdout.WriteString("Closed Deals, RUB\n")
		tblRub.Render()
		os.Stdout.WriteString("\n\n")
	}

	if len(rowsUnclosed) > 0 {
		tblUnclosed := table.NewWriter()
		tblUnclosed.SetColumnConfigs([]table.ColumnConfig{
			{
				Name:        "Ticker",
				Align:       text.AlignLeft,
				AlignFooter: text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Time",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Price",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Amount",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Commission",
				Align:       text.AlignRight,
				AlignFooter: text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
		})
		tblUnclosed.SetOutputMirror(os.Stdout)
		tblUnclosed.AppendHeader(table.Row{"Ticker", "Time", "Price", "Amount", "Commission"})

		for _, row := range rowsUnclosed {
			tblUnclosed.AppendRow(row)
		}

		os.Stdout.WriteString("Opened Positions\n")
		tblUnclosed.Render()
		os.Stdout.WriteString("\n\n")
	}

	if len(rowsClosedDeals) > 0 {
		tblClosedDeals := table.NewWriter()
		tblClosedDeals.SetColumnConfigs([]table.ColumnConfig{
			{
				Name:        "Ticker",
				Align:       text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Amount",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},

			{
				Name:        "Bought At",
				Align:       text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Buy Price",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Comm.",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Buy Cost",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},

			{
				Name:        "-",
				Align:       text.AlignCenter,
				AlignHeader: text.AlignCenter,
			},

			{
				Name:        "Sold At",
				Align:       text.AlignLeft,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Sell Price",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Comm.",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
			{
				Name:        "Sell Cost",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},

			{
				Name:        "Profit",
				Align:       text.AlignRight,
				AlignHeader: text.AlignCenter,
			},
		})

		tblClosedDeals.SetOutputMirror(os.Stdout)
		tblClosedDeals.AppendHeader(table.Row{"Ticker", "Amount",
			"Bought At", "Buy Price", "Comm.", "Buy Cost",
			"-",
			"Sold At", "Sell Price", "Comm.", "Sell Cost",
			"Profit",
		})

		for _, row := range rowsClosedDeals {
			tblClosedDeals.AppendRow(row)
		}

		os.Stdout.WriteString("Deals Log\n")
		tblClosedDeals.Render()
		os.Stdout.WriteString("\n")
	}
}

func value(v float64, isUsd bool, printEmpty bool, withColors bool) string {
	if v == 0 && printEmpty {
		return ""
	}

	var valueStr string
	if isUsd {
		valueStr = fmt.Sprintf("%.2f$", v)
	} else {
		valueStr = fmt.Sprintf("%.4fP", v)
	}

	if withColors {
		if v < 0 {
			valueStr = red(valueStr)
		} else if v > 0 {
			valueStr = green(valueStr)
		} else {
			valueStr = white(valueStr)
		}
	}

	return valueStr
}

func PrintAllTickerDeals(ticker string) {
	PrintTickerDealsFromToNow(ticker, time.Date(2018, 1, 1, 10, 0, 0, 0, time.Local))
}

func PrintTickerDealsFromDate(ticker string, date string) {
	t := config.GetDate(date)

	from := time.Date(t.Year(), t.Month(), t.Day(), 7, 0, 0, 0, time.UTC)
	to := time.Now().UTC()

	PrintTickerDeals(ticker, from, to)
}

func PrintTickerDealsFromToNow(ticker string, from time.Time) {
	PrintTickerDeals(ticker, from, time.Now())
}

func PrintTickerDeals(ticker string, from time.Time, to time.Time) {
	dealsInfo, tradesInfo, err := deals.GetDealsFilteredByOpenTime(ticker, from, to)

	if err != nil {
		log.Fatalln(err)
	}

	if tradesInfo.GetTradesCount() > 0 {
		log.Println("Unprocessed deals:")
		tradesInfo.PrintTrades()
		tradesInfo.Print()
	} else {
		log.Println("No unprocessed deals")
	}

	if dealsInfo.GetDealsCount() > 0 {
		log.Println("Closed deals:")
		dealsInfo.PrintDeals()
		dealsInfo.Print()
	} else {
		log.Println("No closed deals")
	}

	zeroProfitAvgPrice := dealsInfo.GetZeroProfitAvgPrice(tradesInfo)
	if zeroProfitAvgPrice != 0 {
		log.Printf("Avg price for zero profit: %.4f, amount: %d\n", zeroProfitAvgPrice, tradesInfo.GetAmount())
	}
}

func PrintTickerDealsExactForPeriod(ticker string, from time.Time, to time.Time) {
	dealsInfo, tradesInfo, err := deals.GetDeals(ticker, from, to)

	if err != nil {
		log.Fatalln(err)
	}

	if tradesInfo.GetTradesCount() > 0 {
		log.Println("Unprocessed deals:")
		tradesInfo.PrintTrades()
		tradesInfo.Print()
	} else {
		log.Println("No unprocessed deals")
	}

	if dealsInfo.GetDealsCount() > 0 {
		log.Println("Closed deals:")
		dealsInfo.PrintDeals()
		dealsInfo.Print()
	} else {
		log.Println("No closed deals")
	}

	zeroProfitAvgPrice := dealsInfo.GetZeroProfitAvgPrice(tradesInfo)
	if zeroProfitAvgPrice != 0 {
		log.Printf("Avg price for zero profit: %.4f, amount: %d\n", zeroProfitAvgPrice, tradesInfo.GetAmount())
	}
}

package deals

import (
	"fmt"
	"log"
	"sort"
	"time"
)

type deal struct {
	buyTrade  trade
	sellTrade trade

	amount     int
	commission float64
	profit     float64
	closedAt   time.Time
}

type dealsInfo struct {
	usd        bool
	dealsCount int
	amount     int
	maxProfit  float64
	minProfit  float64
	profit     float64
	commission float64
	deals      []deal
}

type dailyStat struct {
	dailyDayInfo map[string]dealsInfo
}

func newDeal(t1 trade, t2 trade) *deal {

	deal := new(deal)

	if t1.isBuying() {
		deal.buyTrade = t1
		deal.sellTrade = t2
	} else {
		deal.buyTrade = t2
		deal.sellTrade = t1
	}

	deal.amount = deal.buyTrade.amount - deal.sellTrade.amount
	deal.commission = deal.buyTrade.commission + deal.sellTrade.commission
	deal.profit = deal.commission -
		deal.sellTrade.getCost() -
		deal.buyTrade.getCost()

	if deal.buyTrade.time.Before(deal.sellTrade.time) {
		deal.closedAt = deal.sellTrade.time
	} else {
		deal.closedAt = deal.buyTrade.time
	}

	return deal
}

func newDealsInfo(deals []deal, usd bool) *dealsInfo {
	dealsInfo := new(dealsInfo)
	dealsInfo.usd = usd

	if len(deals) == 0 {
		return dealsInfo
	}

	dealsInfo.deals = deals

	dealsInfo.amount = dealsInfo.deals[0].amount
	dealsInfo.maxProfit = dealsInfo.deals[0].profit
	dealsInfo.minProfit = dealsInfo.deals[0].profit
	dealsInfo.profit += dealsInfo.deals[0].profit
	dealsInfo.commission += dealsInfo.deals[0].commission
	dealsInfo.dealsCount = len(dealsInfo.deals)

	for _, d := range deals[1:] {
		if dealsInfo.maxProfit < d.profit {
			dealsInfo.maxProfit = d.profit
		}

		if dealsInfo.minProfit > d.profit {
			dealsInfo.minProfit = d.profit
		}

		dealsInfo.amount += d.amount
		dealsInfo.profit += d.profit
		dealsInfo.commission += d.commission
	}

	return dealsInfo
}

func newDailyDealsInfo(info dealsInfo) *dailyStat {

	dailyDealsInfo := make(map[string]dealsInfo)

	//for _, deal := range info.deals {
	//	dayStr := deal.getCloseTime().Format(config.DateFormat)
	//
	//	savedDealsInfo, found := dailyDealsInfo[dayStr]
	//	if found {
	//		deals
	//		savedDealsInfo.deals
	//	}
	//}

	dailyStat := new(dailyStat)
	dailyStat.dailyDayInfo = dailyDealsInfo

	return dailyStat
}

func (d deal) getPricesDiff() float64 {
	return d.sellTrade.price - d.buyTrade.price
}

func (d deal) getOpenTime() time.Time {
	if d.buyTrade.time.Before(d.sellTrade.time) {
		return d.buyTrade.time
	} else {
		return d.sellTrade.time
	}
}

func (d deal) getCloseTime() time.Time {
	if d.buyTrade.time.After(d.sellTrade.time) {
		return d.buyTrade.time
	} else {
		return d.sellTrade.time
	}
}

func (info dealsInfo) GetDealsCount() int {
	return info.dealsCount
}

func (info dealsInfo) GetProfit() float64 {
	return info.profit
}

func (info dealsInfo) GetCommission() float64 {
	return info.commission
}

func (info dealsInfo) IsUSD() bool {
	return info.usd
}

func (info dealsInfo) GetMaxProfit() float64 {
	return info.maxProfit
}

func (info dealsInfo) GetMinProfit() float64 {
	return info.minProfit
}

func (info dealsInfo) GetDeals() []deal {
	return info.deals
}

func (info dealsInfo) sortDeals() {
	sort.Slice(info.deals, func(i, j int) bool {
		return info.deals[i].getOpenTime().Before(info.deals[j].getOpenTime())
	})
}

func (info dealsInfo) Print() {
	log.Printf("\n==================\n"+
		"Deals count:\t%d\n"+
		"Amount:\t%d\n"+
		"Profit:\t\t\t%.4f\n"+
		"Comission:\t\t%.4f\n"+
		"==================\n",
		info.dealsCount,
		info.amount,
		info.profit,
		info.commission)
}

func (info dealsInfo) PrintDeals() {
	for _, d := range info.deals {
		log.Println(d.ToString())
	}
}

func (d deal) GetBuyTrade() trade {
	return d.buyTrade
}

func (d deal) GetSellTrade() trade {
	return d.sellTrade
}

func (d deal) GetComission() float64 {
	return d.commission
}

func (d deal) GetProfit() float64 {
	return d.profit
}

func (d deal) IsLongDeal() bool {
	return d.closedAt.After(d.buyTrade.time)
}

func (d deal) ToString() string {
	return fmt.Sprintf("[%s] - [%s]; commission:%.2f; profit:%.4f",
		d.buyTrade.ToString(), d.sellTrade.ToString(),
		d.commission, d.profit)
}

func (info dealsInfo) GetZeroProfitAvgPrice(tInfo *tradesInfo) float64 {
	if tInfo.GetTradesCount() > 0 {
		return tInfo.avgPrice - info.profit/float64(tInfo.amount)
	}
	return 0.0
}

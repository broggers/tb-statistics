package deals

import (
	sdk "github.com/TinkoffCreditSystems/invest-openapi-go-sdk"
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
	"gitlab.com/ogornostaev/tb-statistics/internal/helper"
	"sort"
	"time"
)

type tradesHolder struct {
	uncompleted []trade
	completed   []deal
}

func (holder *tradesHolder) appendUncompleted(t trade) {
	holder.uncompleted = append(holder.uncompleted, t)
}

func (holder *tradesHolder) appendCompleted(d deal) {
	holder.completed = append(holder.completed, d)
}

func (holder *tradesHolder) removeFromUncompleted(idx int) {
	holder.uncompleted = append(holder.uncompleted[:idx], holder.uncompleted[idx+1:]...)
}

type dealCandidateCtx struct {
	indexUncompletedTrade int
	dealCandidate         *deal
}

func throwError(err error) (_dealsInfo *dealsInfo, _tradesInfo *tradesInfo, _err error) {
	return new(dealsInfo), new(tradesInfo), err
}

func GetDealsFilteredByCloseTime(ticker string, from time.Time, to time.Time) (*dealsInfo, *tradesInfo, error) {
	dealsInfo, tradesInfo, err := GetDeals(ticker, config.StartTradesTime, to)
	if err != nil {
		return throwError(err)
	}

	filteredDeals := make([]deal, 0)
	for _, d := range dealsInfo.deals {
		if d.getCloseTime().After(from) {
			filteredDeals = append(filteredDeals, d)
		}
	}

	filteredTrades := make([]trade, 0)
	for _, t := range tradesInfo.trades {
		if t.time.After(from) {
			filteredTrades = append(filteredTrades, t)
		}
	}

	dealsInfo = newDealsInfo(filteredDeals, dealsInfo.usd)
	tradesInfo = newTradesInfo(filteredTrades)

	return dealsInfo, tradesInfo, nil
}

func GetDealsFilteredByOpenTime(ticker string, from time.Time, to time.Time) (*dealsInfo, *tradesInfo, error) {

	dealsInfo, tradesInfo, err := GetDeals(ticker, config.StartTradesTime, to)
	if err != nil {
		return throwError(err)
	}

	filteredDeals := make([]deal, 0)
	for _, d := range dealsInfo.deals {
		if d.getOpenTime().After(from) {
			filteredDeals = append(filteredDeals, d)
		}
	}

	filteredTrades := make([]trade, 0)
	for _, t := range tradesInfo.trades {
		if t.time.After(from) {
			filteredTrades = append(filteredTrades, t)
		}
	}

	dealsInfo = newDealsInfo(filteredDeals, dealsInfo.usd)
	tradesInfo = newTradesInfo(filteredTrades)

	return dealsInfo, tradesInfo, nil

}

func GetDeals(ticker string, from time.Time, to time.Time) (*dealsInfo, *tradesInfo, error) {

	tradesHolder := new(tradesHolder)

	tradesHolder.uncompleted = make([]trade, 0)
	tradesHolder.completed = make([]deal, 0)
	usd := true

	operations, err := helper.GetSortedOperations(ticker, from, to)
	if err != nil {
		return throwError(err)
	}

	for _, operation := range operations {

		if operation.Status == sdk.OperationStatusDecline {
			continue
		}

		if operation.OperationType == sdk.OperationTypeDividend {
			//dividend += operation.Payment
		} else if helper.IsBuyOrSell(&operation) {

			usd = operation.Currency == sdk.USD

			for _, _trade := range buildTrades(operation) {
				//fmt.Println(_trade.ToString())
				processTrade(_trade, tradesHolder)
			}
		}
	}

	dealsInfo := newDealsInfo(tradesHolder.completed, usd)
	dealsInfo.sortDeals()
	tradesInfo := newTradesInfo(tradesHolder.uncompleted)

	return dealsInfo, tradesInfo, nil
}

func findUncompletedDeal(trade trade, holder *tradesHolder) (uncompletedIdx int) {

	suitableDeals := make([]dealCandidateCtx, 0)

	for idx, uncompletedTrade := range holder.uncompleted {
		if trade.isBuying() != uncompletedTrade.isBuying() &&
			trade.time.After(uncompletedTrade.time) {
			var dealCandidate *deal

			if trade.isBuying() {
				dealCandidate = newDeal(trade, uncompletedTrade)
			} else {
				dealCandidate = newDeal(uncompletedTrade, trade)
			}

			suitableDeals = append(suitableDeals, dealCandidateCtx{idx, dealCandidate})
		}
	}

	if len(suitableDeals) == 0 {
		return -1
	}

	sort.Slice(suitableDeals, func(i, j int) bool {
		return suitableDeals[i].dealCandidate.getPricesDiff() > suitableDeals[j].dealCandidate.getPricesDiff()
	})

	return suitableDeals[0].indexUncompletedTrade
}

func processTrade(_trade trade, holder *tradesHolder) {
	uncompletedIdx := findUncompletedDeal(_trade, holder)

	if uncompletedIdx == -1 {
		holder.appendUncompleted(_trade)
		return
	}

	suitableTrade := holder.uncompleted[uncompletedIdx]
	//log.Printf("found uncompleted deal: %v\n", pSuitableDeal.print())

	var deal *deal
	if suitableTrade.getAbsAmount() == _trade.getAbsAmount() {
		if _trade.isBuying() {
			deal = newDeal(_trade, suitableTrade)
		} else {
			deal = newDeal(suitableTrade, _trade)
		}

		holder.appendCompleted(*deal)
		holder.removeFromUncompleted(uncompletedIdx)

	} else if suitableTrade.getAbsAmount() > _trade.getAbsAmount() {

		restTrade := suitableTrade
		restTrade.amount = restTrade.amount + _trade.amount
		restTrade.commission = 0.0

		suitableTrade.amount = -_trade.amount

		if _trade.isBuying() {
			deal = newDeal(_trade, suitableTrade)
		} else {
			deal = newDeal(suitableTrade, _trade)
		}

		holder.appendCompleted(*deal)
		holder.removeFromUncompleted(uncompletedIdx)
		holder.appendUncompleted(restTrade)
	} else {
		restTrade := _trade
		restTrade.commission = 0.0
		restTrade.amount = restTrade.amount + suitableTrade.amount

		_trade.amount = -suitableTrade.amount

		if _trade.isBuying() {
			deal = newDeal(_trade, suitableTrade)
		} else {
			deal = newDeal(suitableTrade, _trade)
		}

		holder.appendCompleted(*deal)
		holder.removeFromUncompleted(uncompletedIdx)

		processTrade(restTrade, holder)
	}
}

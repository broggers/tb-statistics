package deals

import (
	"fmt"
	sdk "github.com/TinkoffCreditSystems/invest-openapi-go-sdk"
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
	"log"
	"time"
)

type trade struct {
	price      float64
	amount     int
	commission float64
	time       time.Time
}

type tradesInfo struct {
	tradesCount int
	avgPrice    float64
	amount      int
	cost        float64
	commission  float64
	trades      []trade
}

func (t trade) GetPrice() float64 {
	return t.price
}

func (t trade) GetAmount() int {
	return t.amount
}

func (t trade) GetCommission() float64 {
	return t.commission
}

func (t trade) GetTime() time.Time {
	return t.time
}

func (t trade) isBuying() bool {
	return t.amount > 0
}

func (t trade) getAbsAmount() int {
	if t.amount < 0 {
		return -t.amount
	}

	return t.amount
}

func (t trade) getCost() float64 {
	return float64(t.amount) * t.price
}

func (t trade) GetCostWithCommission() float64 {
	return float64(t.amount)*t.price - t.commission
}

func (t trade) ToString() string {
	return fmt.Sprintf("%s; %.4f; %d; %.2f",
		t.time.Format(config.TimeFormat),
		t.price,
		t.amount,
		t.commission)
}

func buildTrades(o sdk.Operation) map[float64]trade {

	tradesByPrice := make(map[float64]trade)

	for _, t := range o.Trades {

		savedTrade, found := tradesByPrice[t.Price]
		if found {
			if o.OperationType == sdk.BUY {
				savedTrade.amount += t.Quantity
			} else {
				savedTrade.amount -= t.Quantity
			}

			tradesByPrice[t.Price] = savedTrade
		} else {
			if o.OperationType == sdk.BUY {
				tradesByPrice[t.Price] = trade{amount: t.Quantity, price: t.Price, time: t.DateTime}
			} else {
				tradesByPrice[t.Price] = trade{amount: -t.Quantity, price: t.Price, time: t.DateTime}
			}
		}
	}

	commissionSet := false
	for _p, _trade := range tradesByPrice {

		if !commissionSet {
			_trade.commission = o.Commission.Value
			tradesByPrice[_p] = _trade
			commissionSet = true
		}
	}

	return tradesByPrice
}

func newTradesInfo(trades []trade) *tradesInfo {
	tradesInfo := new(tradesInfo)

	if len(trades) == 0 {
		return tradesInfo
	}

	tradesInfo.trades = trades

	tradesInfo.avgPrice = 0.0
	tradesInfo.amount = 0
	tradesInfo.cost = 0.0
	tradesInfo.commission = 0.0
	tradesInfo.tradesCount = len(tradesInfo.trades)

	for _, t := range trades {
		tradesInfo.cost += t.GetCostWithCommission()
		tradesInfo.amount += t.getAbsAmount()
		tradesInfo.commission += t.commission
	}

	if tradesInfo.amount > 0 {
		tradesInfo.avgPrice = tradesInfo.cost / float64(tradesInfo.amount)
	}

	return tradesInfo
}

func (info tradesInfo) GetTradesCount() int {
	return info.tradesCount
}

func (info tradesInfo) GetAmount() int {
	return info.amount
}

func (info tradesInfo) GetCost() float64 {
	return info.cost
}

func (info tradesInfo) GetCommission() float64 {
	return info.commission
}

func (info tradesInfo) GetAvgPrice() float64 {
	return info.avgPrice
}

func (info tradesInfo) GetTrades() []trade {
	return info.trades
}

func (info tradesInfo) Print() {
	log.Printf("\n==================\n"+
		"Trades count:\t%d\n"+
		"Cost:\t\t\t%.4f\n"+
		"Amount:\t\t\t%d\n"+
		"Commission:\t\t%.4f\n"+
		"AvgPrice:\t\t%.4f\n"+
		"==================\n",
		info.tradesCount,
		info.cost, info.amount,
		info.commission, info.avgPrice)
}

func (info tradesInfo) PrintTrades() {
	for _, t := range info.trades {
		log.Println(t.ToString())
	}
}

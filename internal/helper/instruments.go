package helper

import (
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
)

var (
	byFIGIMap   map[string]string
	byTickerMap map[string]string
)

func init() {
	byFIGIMap = make(map[string]string)
	byTickerMap = make(map[string]string)
}

func GetTickerByFIGI(figi string) string {

	if figi == "" {
		return "N/A"
	}

	client, ctx, cancel := config.GetClient()
	defer cancel()

	ticker, found := byFIGIMap[figi]
	if found {
		return ticker
	} else {
		instrument, err := client.SearchInstrumentByFIGI(ctx, figi)
		if err != nil {
			return "N/A"
		}

		byFIGIMap[figi] = instrument.Ticker
		return instrument.Ticker
	}
}

func GetFIGIByTicker(ticker string) string {

	if ticker == "" {
		return "N/A"
	}

	client, ctx, cancel := config.GetClient()
	defer cancel()

	figi, found := byTickerMap[ticker]
	if found {
		return figi
	} else {
		instrument, err := client.SearchInstrumentByTicker(ctx, ticker)
		if err != nil {
			return "N/A"
		}

		byFIGIMap[ticker] = instrument[0].FIGI
		return instrument[0].FIGI
	}
}

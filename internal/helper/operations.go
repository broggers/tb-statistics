package helper

import (
	"fmt"
	sdk "github.com/TinkoffCreditSystems/invest-openapi-go-sdk"
	"github.com/google/uuid"
	"gitlab.com/ogornostaev/tb-statistics/internal/config"
	"io"
	"log"
	"sort"
	"time"
)

type Operation struct {
	id                 string
	dt                 time.Time
	operationType      string
	currency           string
	ticker             string
	cost               float64
	amount             int
	price              float64
	commission         float64
	commissionCurrency string
}

func throwError(err error) (_operations []sdk.Operation, _err error) {
	return []sdk.Operation{}, err
}

func throwSoftError() (_operations []sdk.Operation, _err error) {
	return []sdk.Operation{}, nil
}

func GetUniqueTickers(from time.Time, to time.Time) ([]string, error) {
	operations, err := GetOperations("", from, to)
	if err != nil {
		return []string{}, err
	}

	client, ctx, cancel := config.GetClient()
	defer cancel()

	keys := make(map[string]bool)
	list := make([]string, 0)

	for _, operation := range operations {

		if !IsBuyOrSell(&operation) {
			continue
		}

		entry := operation.FIGI
		if _, value := keys[entry]; !value {
			keys[entry] = true

			instrument, err := client.SearchInstrumentByFIGI(ctx, entry)
			if err != nil {
				return []string{}, err
			}

			list = append(list, instrument.Ticker)
		}
	}

	return list, nil
}

func GetSortedOperations(ticker string, from time.Time, to time.Time) ([]sdk.Operation, error) {
	operations, err := GetOperations(ticker, from, to)

	if err != nil {
		return throwSoftError()
	}

	if len(operations) > 0 {
		sort.Slice(operations, func(i, j int) bool {
			return operations[i].DateTime.Before(operations[j].DateTime)
		})
	}

	return operations, nil
}

func GetOperations(ticker string, from time.Time, to time.Time) (_operations []sdk.Operation, _err error) {
	client, ctx, cancel := config.GetClient()
	defer cancel()

	var (
		operations []sdk.Operation
		err        error
	)

	if ticker == "" {
		operations, err = client.Operations(ctx, from, to, "")

		if err != nil {
			log.Println("unable to get all operations")
			return throwSoftError()
		}
	} else {
		instruments, err := client.SearchInstrumentByTicker(ctx, ticker)
		if err != nil {
			log.Println("unable to search instrument by ticker: " + ticker)
			return throwSoftError()
		}

		if len(instruments) == 0 {
			log.Println("unknown ticker: " + ticker)
			return throwSoftError()
		}

		operations, err = client.Operations(ctx, from, to, instruments[0].FIGI)

		if err != nil {
			log.Println("unable to get operations by ticker: " + ticker)
			return throwSoftError()
		}
	}

	filtered := operations[:0]
	for _, operation := range operations {
		if operation.Status != sdk.OperationStatusDecline &&
			operation.OperationType != sdk.OperationTypeBrokerCommission &&
			!operation.DateTime.Before(from) &&
			!operation.DateTime.After(to) {
			filtered = append(filtered, operation)
		}
	}

	return filtered, nil
}

func ToOperations(sdkOperation sdk.Operation) []*Operation {
	operations := make([]*Operation, 0)

	if IsBuyOrSell(&sdkOperation) {
		operationsByPrice := make(map[float64]*Operation)

		for _, t := range sdkOperation.Trades {

			savedOperation, found := operationsByPrice[t.Price]
			if found {
				savedOperation.amount += t.Quantity
				operationsByPrice[t.Price] = savedOperation
			} else {
				operation := new(Operation)
				operation.id = uuid.New().String()
				operation.dt = t.DateTime
				operation.operationType = string(sdkOperation.OperationType)
				operation.currency = string(sdkOperation.Currency)
				operation.ticker = GetTickerByFIGI(sdkOperation.FIGI)
				operation.cost = float64(t.Quantity) * t.Price
				operation.amount = t.Quantity
				operation.price = t.Price
				operation.commission = 0.0
				operation.commissionCurrency = string(sdkOperation.Commission.Currency)

				operationsByPrice[t.Price] = operation
			}
		}

		commissionSet := false
		for price, operation := range operationsByPrice {
			if !commissionSet {
				operation.commission = sdkOperation.Commission.Value
				operationsByPrice[price] = operation
				commissionSet = true
			}

			operations = append(operations, operation)
			if len(sdkOperation.Trades) == 1 && operation.commission == 0 {
				log.Println(sdkOperation)
				log.Println(operation)
			}
		}

	} else {

		operation := new(Operation)

		operation.id = uuid.New().String()
		operation.dt = sdkOperation.DateTime
		operation.operationType = string(sdkOperation.OperationType)
		operation.currency = string(sdkOperation.Currency)
		operation.ticker = GetTickerByFIGI(sdkOperation.FIGI)
		operation.cost = sdkOperation.Payment
		operation.amount = sdkOperation.Quantity
		operation.price = sdkOperation.Price
		operation.commission = sdkOperation.Commission.Value
		operation.commissionCurrency = string(sdkOperation.Commission.Currency)

		operations = append(operations, operation)
	}

	return operations
}

func PrintOperation(operation sdk.Operation, writer io.Writer) error {
	var str string

	if IsBuyOrSell(&operation) {
		str = ""
		commissionSet := false

		for _, trade := range operation.Trades {

			str += fmt.Sprintf("%v;%v;%v;%v;%v;%v;%v;%v;%v\n",
				operation.DateTime.Format(config.TimeFormat),
				operation.OperationType,
				operation.Currency,
				GetTickerByFIGI(operation.FIGI),
				float64(trade.Quantity)*trade.Price,
				trade.Quantity,
				trade.Price,
				config.IfThenElse(commissionSet, 0.0, operation.Commission.Value),
				operation.Commission.Currency)

			commissionSet = true
		}
	} else {
		str = fmt.Sprintf("%v;%v;%v;%v;%v;%v;%v;%v;%v\n",
			operation.DateTime.Format(config.TimeFormat),
			operation.OperationType,
			operation.Currency,
			GetTickerByFIGI(operation.FIGI),
			operation.Payment,
			operation.Quantity,
			operation.Price,
			operation.Commission.Value,
			operation.Commission.Currency)
	}

	_, err := writer.Write([]byte(str))

	return err
}

func IsBuy(operation *sdk.Operation) bool {
	return operation.OperationType == sdk.BUY ||
		string(operation.OperationType) == "BuyCard"
}

func IsSell(operation *sdk.Operation) bool {
	return operation.OperationType == sdk.SELL
}

func IsBuyOrSell(operation *sdk.Operation) bool {
	return IsSell(operation) || IsBuy(operation)
}

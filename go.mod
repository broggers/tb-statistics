module gitlab.com/ogornostaev/tb-statistics

go 1.14

require (
	github.com/TinkoffCreditSystems/invest-openapi-go-sdk v0.3.0
	github.com/cheggaaa/pb/v3 v3.0.5
	github.com/go-openapi/strfmt v0.19.6 // indirect
	github.com/google/uuid v1.1.1
	github.com/gookit/color v1.3.0
	github.com/jedib0t/go-pretty v4.3.0+incompatible
)
